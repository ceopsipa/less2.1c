FROM python:3

ENV PYTHONUNBUFFERED=1
WORKDIR /app
RUN git clone https://gitlab.com/chumkaska1/django_blog.git .
RUN pip install -r requirements.txt
